//librerias
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
    //clase abstracta que hereda de bebidacomponente 
    public abstract class AgregadoDecorator : bebidacomponente
    {
        //durante la construcción de un agregado vamos
        // a necesitar una instancia bebida 
        protected bebidacomponente _bebida;
        public AgregadoDecorator(bebidacomponente bebida)
        {
            _bebida = bebida;
        }
    }
}
