//librerias
using System;
using System.Collections.Generic;
using System.Text;

//nombre del proyecto
namespace carritodecompras
{
    class Producto
    {
        public Producto()
        {
        }

        public int Identificador { get; set; }

        public string Descripcion { get; set; }

        public decimal Precio { get; set; }
    }
}
