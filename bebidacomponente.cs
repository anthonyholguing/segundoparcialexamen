using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
    //tenemos una clase clase abstracta
    public abstract class bebidacomponente
    {
        //tiene dos operaciones abstracta de Costoo y Despcripción
        public abstract double Costo { get; }
        public abstract string Descripcion { get; }
    }
}
