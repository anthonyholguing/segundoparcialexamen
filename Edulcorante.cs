//librerias
using System;
using System.Collections.Generic;
using System.Text;


namespace carritodecompras
{
    public class Edulcorante :AgregadoDecorator
    {
        public Edulcorante(bebidacomponente bebida) : base(bebida) { }
        public override double Costo => _bebida.Costo + 1;
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Edulcorante");
    }
}