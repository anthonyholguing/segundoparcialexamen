//librerias
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
//nombre del proyecto
namespace carritodecompras
{
    //la clase leche hereda de AgregadoDecorator
    //vamos a definir de la clase bebidacomponente y la 
    //bebida será la enviemos a la bebida abstracta
    public class leche : AgregadoDecorator
    {
        public leche(bebidacomponente bebida) : base(bebida) { }
       //vamos agregarle el costo
        public override double Costo => _bebida.Costo + 2;
       //
        public override string Descripcion => string.Format($"{_bebida.Descripcion},leche");
    }

}
