//librerias
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
    public class Crema : AgregadoDecorator
    {
        public Crema(bebidacomponente bebida) : base(bebida) { }
        public override double Costo => _bebida.Costo + 4;
        public override string Descripcion => string.Format($"{_bebida.Descripcion},Crema");
    }
}
