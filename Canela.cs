//librerias
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
   public class Canela : AgregadoDecorator
    {
        public Canela(bebidacomponente bebida) : base(bebida) { }
        public override double Costo => _bebida.Costo + 1.75;
        public override string Descripcion => string.Format($"{_bebida.Descripcion},Canela");
    }
}
